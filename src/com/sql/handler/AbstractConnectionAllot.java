package com.sql.handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import com.sql.pool.IBuilder;

public abstract class AbstractConnectionAllot {
	
	/**
	 * 初始化一次即可
	 * @param driverClassName
	 * @param url
	 * @param user
	 * @param password
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 */
	protected void init(Deploy deploy) throws ClassNotFoundException, IllegalAccessException, SQLException, IOException {
		if(deploy != null){
			BuilderInstance.init(deploy); 
		}
	}
	
	/**
	 * 对连接进行回收，重新利用。
	 * 当用户调用SQLUtil方法中的insert、select、delect、update等方法，不需要进行回收。
	 * @param con
	 * @throws ClassNotFoundException
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws FileNotFoundException 
	 */
	protected  void release(Connection con) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
			getBuilder().builderRelease(con);
	}
	
	/**
	 * 得到由核心类创建的连接
	 * 注意：此方法是可以供对象调用的，并且是静态方法，这极大的增加了程序的灵活性
	 * 我们可以自己得到连接，来进行数据库的操作
	 * 用完之后建议调用release方法进行回收
	 * @return
	 * @throws SQLException 
	 * @throws InterruptedException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws FileNotFoundException 
	 */
	@SuppressWarnings("resource")
	protected  Connection getConnection() throws ClassNotFoundException, InterruptedException, SQLException, FileNotFoundException, IllegalAccessException, IOException{
				Connection connection = getBuilder().getBuilderConnection();
				if (connection == null || connection.isClosed()){
					return null;
				}
				return connection;
	}
	
	/**
	 * 得到核心类的对象
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws FileNotFoundException 
	 */
	protected  IBuilder getBuilder() 
			throws ClassNotFoundException, SQLException, FileNotFoundException, IllegalAccessException, IOException{
		return BuilderInstance.getInstance();
	}
	
	/**
	 * 为查询单独建立一个连接，查询涉及不到数据的修改、更新，所以对数据没有更改
	 * 可以高并发
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InterruptedException 
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws FileNotFoundException 
	 */
	protected  Connection getSelectConnection() 
			throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
			Connection selectConnection = getBuilder().getBuilderSelectConnection();
			if (selectConnection == null || selectConnection.isClosed()) {
				return null;
			}
			return selectConnection;
	}
	
	/**
	 * 关闭数据库连接池
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws SQLException
	 * @throws IOException
	 */
	protected  void closeConnectionPool() throws ClassNotFoundException, FileNotFoundException, IllegalAccessException, SQLException, IOException{
		BuilderInstance.closeConnectionPool();
	}
	
	/**
	 * 得到目前缓存池中的连接的数量
	 * @return
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws SQLException
	 * @throws IOException	
	 */
	protected  int getCacheConnectionCount() throws ClassNotFoundException, FileNotFoundException, IllegalAccessException, SQLException, IOException{
		return getBuilder().builderCacheCount();
	}
	
	/**
	 * 得到新建连接的数量
	 * @return
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws SQLException
	 * @throws IOException
	 */
	protected  int getNewConnectionCount() throws ClassNotFoundException, FileNotFoundException, IllegalAccessException, SQLException, IOException{
		return getBuilder().builderNewCacheCount();
	}
	
}
