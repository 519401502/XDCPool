package com.sql.handler.util;

import java.util.Vector;


public class Arrays<T1,T2> implements Cloneable {
	
	public volatile Vector<T1> connections = new Vector<>();
	public volatile Vector<T2> times = new Vector<>();
	
	public synchronized void add(T1 t1,T2 t2){
		connections.add(t1);
		times.add(t2);
	}
	
	public synchronized void remove(T1 t1){
		for(int i = 0;i < connections.size();i++){
			if(ObjEqual.objIsEqual(t1, connections.get(i))){
				connections.remove(i);
				times.remove(i);
			}
		}
	}
	
	public synchronized ArrayItem<T1, T2> remove(int index){
		if(index >= connections.size()){
			return null;
		}
		return new ArrayItem<>(connections.remove(index),times.remove(index));
	}
	
	public int size(){
		int connectionsSize = connections.size();
		int timesSize = times.size();
		if(connectionsSize == timesSize){
			return connectionsSize;
		}
		return 0;
	}
	
	public ArrayItem<T1, T2> get(int index){
		return new ArrayItem<>(connections.get(index),times.get(index));
	}
	
	public synchronized void removeAll(){
		connections.clear();
		times.clear();
	}
	
	@SuppressWarnings("unchecked")
	public Arrays<T1, T2> clone(){
		try {
			Arrays<T1,T2> v = (Arrays<T1,T2>)super.clone();
            v.connections = (Vector<T1>)connections.clone();
            v.times = (Vector<T2>)times.clone();
            return v;
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e);
        }
	}
	
}
