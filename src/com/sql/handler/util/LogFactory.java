package com.sql.handler.util;

public class LogFactory {
	
	public static ILogs create(boolean isClose){
		if(!isClose){
			   return new Logs();
		   } else {
			   return new LogsEmpty();
		   }
	}
	
}
