package com.sql.handler.util;

public class ObjEqual {
	
	public static <T, T2> boolean objIsEqual(T obj1,T2 obj2){
		if(obj1 == null || obj2 == null){
			return false;
		}
		if(obj1.toString().equals(obj2.toString())){
			return true;
		}
		return false;
	}

}
