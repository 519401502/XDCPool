package com.sql.handler.util;

public interface ILogs {
	
	public void setTAB(String aTAB);
	
	public void d(String string);
	
	public  void d(int number);
	
	public  void d(double number);
	
	public  void d(float number);
	
	public  void d(char ch);
	
	public  void d(boolean isboolean);
	
}
