package com.sql.handler.util;

public class Logs implements ILogs{
	
	private String TAB = "SQLPool:";
	
	public void setTAB(String aTAB){

			TAB = aTAB;

	}
	
	public void d(String string){

			System.out.println(TAB + string);

	}
	
	public  void d(int number){

			System.out.println(TAB + number);

	}
	
	public  void d(double number){

			System.out.println(TAB + number);

	}
	
	public  void d(float number){

			System.out.println(TAB + number);

	}
	
	public  void d(char ch){

			System.out.println(TAB + ch);

	}
	
	public  void d(boolean isboolean){

			System.out.println(TAB + isboolean);

	}
}
