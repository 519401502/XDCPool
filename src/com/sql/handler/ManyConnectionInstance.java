package com.sql.handler;

public class ManyConnectionInstance {
	
	private static ManyConnectionHandler handler;

	protected static ManyConnectionHandler getInstance() {
		if(handler != null){
			return handler;
		}
		synchronized(ManyConnectionInstance.class){
			if(handler == null){
				handler = new ManyConnectionHandler();
			}
		}
		return handler;
	}
}
