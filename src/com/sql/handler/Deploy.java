package com.sql.handler;

import java.io.Serializable;

public class Deploy implements Serializable {
	
	/*
	 * 序列化ID
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * 缓存池中的缓存连接的数量
	 */
	private int CacheSize;
	
	/*
	 * 最大新建连接数
	 */
	private int NewMaxSize;
	
	/*
	 * 数据库的url
	 */
	private String url = null;
	
	/*
	 * 数据库账户名
	 */
	private String user = null;
	
	/*
	 * 数据库密码
	 */
	private String password = null;
	
	/*
	 * 数据库驱动名称
	 */
	private String DRIVER_CLASS_NAME = null;
	
	/*
	 * 设置是否开启日志
	 */
	private boolean closeLog = true;
	
	/*
	 * 是否存储账户信息
	 */
	private boolean isSaveInfo = false;
	
	public Deploy setIsSaveInfo(boolean isSaveInfo){
		this.isSaveInfo = isSaveInfo;
		return this;
	}
	
	public boolean getIsSaveInfo(){
		return isSaveInfo;
	}
	
	public Deploy setCloseLog(boolean isCloseLog){
		closeLog = isCloseLog;
		return this;
	}
	
	public boolean getCloseLog(){
		return closeLog;
	}
	
	public Deploy setCacheSize(int cacheSize){
		CacheSize = cacheSize;
		return this;
	}
	
	public Deploy setDriverClassName(String driverClassName){
		DRIVER_CLASS_NAME = driverClassName;
		return this;
	}
	
	public Deploy setNewMaxSize(int newMaxSize){
		NewMaxSize = newMaxSize;
		return this;
	}
	
	public Deploy setUrl(String url){
		this.url = url;
		return this;
	}
	
	public Deploy setPassword(String password){
		this.password = password;
		return this;
	}
	
	public Deploy setUser(String user){
		this.user = user;
		return this;
	}
	
	public String getDriverClassName(){
		return DRIVER_CLASS_NAME;
	}
	
	public String getUrl(){
		return url;
	}
	
	public String getUser(){
		return user;
	}
	
	public String getPassword(){
		return password;
	}
	
	public int getCacheSize(){
		return CacheSize;
	}
	
	public int getNewMaxSize(){
		return NewMaxSize;
	}

}
