package com.sql.handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import com.sql.handler.util.SerializableUtil;
import com.sql.pool.ChooseConectionPool;
import com.sql.pool.IBuilder;




public class BuilderInstance {
	
	private static IBuilder mBuilder;
	
	public static void init(Deploy deploy) throws ClassNotFoundException, IllegalAccessException, SQLException, IOException{
		if(deploy != null){
			mBuilder = ChooseConectionPool.getIBuilder(deploy);
			SerializableUtil.writeObject(SerializableUtil.FILE_NAME, deploy);
		}
	}
	
	public static IBuilder getInstance() throws FileNotFoundException, ClassNotFoundException, IOException, IllegalAccessException, SQLException {
		if(mBuilder != null){
			return mBuilder;
		} 
		synchronized(BuilderInstance.class){
			if(mBuilder == null){
				Deploy deploy;
				deploy = (Deploy) SerializableUtil.readObject(SerializableUtil.FILE_NAME);
				if(deploy == null){
					new IllegalArgumentException("读取配置信息失败");
				}
				mBuilder = ChooseConectionPool.getIBuilder(deploy);	
			}
		}
		return mBuilder;
	}
	
	public static void closeConnectionPool(){
		if(mBuilder != null){
			mBuilder.builderCloseConnectionPool();
			SerializableUtil.deleteObject(SerializableUtil.FILE_NAME);
			mBuilder = null;
		}
		
	}

}
