package com.sql.handler;

public class SQLHandlerFactor {
	
	private static final String SQLUtil = "SQLUtil"; 
	
	public static ISQLHandler create(String className){
		switch(className){
		case SQLUtil:
			return new SQLHandler();
		}
		return null;
	}
}
