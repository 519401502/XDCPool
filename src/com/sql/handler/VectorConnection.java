package com.sql.handler;

import java.util.Vector;

public class VectorConnection<E> extends Vector<E>{

	private static final long serialVersionUID = 1L;

	/**
	 * 重写父类方法，控制指针越界
	 */
	@Override
	public synchronized E remove(int index) {
		if(super.size() > 0 && super.size() >= index){
			return super.remove(index);
		}
		return null;
	}
	
	@Override
	public synchronized boolean remove(Object o) {
		// TODO Auto-generated method stub
		if(super.size() > 0){
			return super.remove(o);
		}
		return false;
	}
	
	@Override
	public boolean add(E e) {
		// TODO Auto-generated method stub
		return super.add(e);
	}
	
	@Override
	public synchronized E get(int index) {
		if(super.size() >= index && index >=0){
			return super.get(index);
		}
		return null;
	}
	
	@Override
	public int size() {
		return super.size();
	}

}
