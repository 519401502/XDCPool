package com.sql.handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;

public class XDCPool {
	
	/**
	 * 初始化，仅需初始化一次
	 * @param aDeploy
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws SQLException
	 * @throws IOException
	 */
	public static void init(Deploy aDeploy){
			try {
				getSQLContext().init(aDeploy);
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	/**
	 * 删除操作
	 * @param sql
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	public static boolean delete(String sql,String[] indexs) {
		isNull(sql);
			try {
				return getSQLContext().delete(sql, indexs);
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | InterruptedException
					| IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return false;
	}
	
	/**
	 * 更新操作
	 * @param sql
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	public static boolean update(String sql,String[] indexs) {
		isNull(sql);

			try {
				return getSQLContext().update(sql, indexs);
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | InterruptedException
					| IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return false;
	}
	
	/**
	 * 查询操作
	 * @param sql
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	public static ResultSet select(String sql,String[] indexs) {
		isNull(sql);
		
			try {
				return getSQLContext().select(sql, indexs);
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | InterruptedException
					| IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return null;
	}
	
	/**
	 * 插入操作
	 * @param sql
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	public static boolean insert(String sql,String[] indexs) {
		isNull(sql);
		
			try {
				return getSQLContext().insert(sql, indexs);
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | InterruptedException
					| IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return false;
	}
	
	/**
	 * 判断参数是否为空
	 * @param sql
	 * @param indexs
	 */
	private static void isNull(String sql){
		if(sql == null){
			throw new IllegalArgumentException("参数不能为空");
		}
	}
	
	/**
	 * 得到连接
	 * @return
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	public static Connection getConnection() {
			try {
				return getSQLContext().getConnection();
			} catch (ClassNotFoundException | IllegalAccessException | InterruptedException | SQLException
					| IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return null;
	}
	
	/**
	 * 进行回收
	 * @param con
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	public static void release(Connection con) {
			try {
				getSQLContext().release(con);
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | InterruptedException
					| IOException e) {
				e.printStackTrace();
			}
		
	}
	
	/**
	 * 关闭数据库连接池
	 */
	public static void closeConnectionPool(){
			try {
				getSQLContext().closeConnectionPool();
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/**
	 * 得到数据库缓存池中的数量
	 */
	public static int getCacheConnectionCount(){
			try {
				return getSQLContext().getCacheConnectionCount();
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return 0;
	}
	
	/**
	 * 获得新建连接的数量
	 */
	public static int getNewConnectionCount(){
			try {
				return getSQLContext().getNewConnectionCount();
			} catch (ClassNotFoundException | IllegalAccessException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return 0;
	}
	
	private static SQLContext getSQLContext(){
		return new SQLContext(SQLContext.SQLUtil);
	}

}
