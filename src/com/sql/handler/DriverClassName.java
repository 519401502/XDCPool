package com.sql.handler;

public class DriverClassName {

	public static final String MySQL = "com.mysql.jdbc.Driver";
	public static final String SQLServer = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	public static final String Oracle = "oracle.jdbc.driver.OracleDriver";

	private DriverClassName(){}
}
