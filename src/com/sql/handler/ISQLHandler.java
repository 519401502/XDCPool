package com.sql.handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface ISQLHandler {
	
	boolean delete(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException;
	
	boolean update(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException;
	
	ResultSet select(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException;
	
	boolean insert(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException;

}
