package com.sql.handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLContext extends AbstractConnectionAllot{
	
	private ISQLHandler sqlHandler;
	public static final String SQLUtil = "SQLUtil"; 
	
	protected SQLContext(String className){
		sqlHandler = SQLHandlerFactor.create(className);
	}
	
	boolean delete(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
		return sqlHandler.delete(sql, indexs);
	}
	
	boolean update(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
		return sqlHandler.update(sql, indexs);
	}
	
	ResultSet select(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
		return sqlHandler.select(sql, indexs);
	}
	
	boolean insert(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
		return sqlHandler.insert(sql, indexs);
	}
	
	

}
