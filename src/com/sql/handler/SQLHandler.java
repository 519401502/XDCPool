package com.sql.handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class SQLHandler extends AbstractConnectionAllot implements ISQLHandler{

	/**
	 * 删除语句，成功则返回true，失败则返回false
	 * @param sql
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws FileNotFoundException 
	 */
	public boolean delete(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
			Connection con = getConnection();
			PreparedStatement statement = packStatement(con.prepareStatement(sql), indexs);
			statement.execute();
			release(con);
			return true;
	}
	
	/**
	 * 更新语句，成功则返回true，失败则返回false
	 * @param sql
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws FileNotFoundException 
	 */
	public  boolean update(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
		Connection con = getConnection();
		PreparedStatement statement = packStatement(con.prepareStatement(sql), indexs);
		statement.executeUpdate();
		release(con);
		return true;
	}
	
	/**
	 * 查询语句，成功则返回ResultSet,失败则返回null
	 * @param sql
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws FileNotFoundException 
	 */
	public  ResultSet select(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException {
		Connection con = getSelectConnection();
		PreparedStatement statement = packStatement(con.prepareStatement(sql), indexs);
		ResultSet resultSet = statement.executeQuery();
		return resultSet;	
	}
	
	/**
	 * 插入语句，成功则返回true，失败则返回false
	 * @param sql
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws FileNotFoundException 
	 */
	public boolean insert(String sql,String[] indexs) throws ClassNotFoundException, SQLException, InterruptedException, FileNotFoundException, IllegalAccessException, IOException{
		Connection con = getConnection();
		PreparedStatement statement = packStatement(con.prepareStatement(sql), indexs);
		statement.executeUpdate();
		release(con);
		return true;		
	}
	
	/**
	 * 将参数装进statement
	 * @param statement
	 * @param indexs
	 * @return
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 * @throws IllegalAccessException
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws IOException
	 */
	private  PreparedStatement packStatement(PreparedStatement statement,String[] indexs) throws ClassNotFoundException, FileNotFoundException, IllegalAccessException, InterruptedException, SQLException, IOException{
		if(indexs != null){
			for(int index = 1;index <= indexs.length; index++){
				statement.setString(index, indexs[index-1]);
			}
		}
		return statement;
	}

}
