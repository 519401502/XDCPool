package com.sql.pool;

import java.sql.Connection;

public interface IBuilder {
	
	void builderRelease(Connection con);
	
	int builderCacheCount();
	
	int builderNewCacheCount();
	
	void builderCloseConnectionPool();
	
	Connection getBuilderSelectConnection();

	Connection getBuilderConnection();
	
}
