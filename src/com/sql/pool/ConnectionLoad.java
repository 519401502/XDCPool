package com.sql.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConnectionLoad {

	protected static Connection load(final String aClassName,final String aURL,final String aUserName,final String aPassword) {		
		if(aClassName == null 
				|| aURL == null
				|| aUserName == null
				|| aPassword == null){
				throw new IllegalArgumentException("必要参数不能为空");
		}
		try {
			Class.forName(aClassName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			return DriverManager.getConnection(aURL, aUserName, aPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }
}
