package com.sql.pool;

import java.io.IOException;
import java.sql.SQLException;

import com.sql.handler.Deploy;


public class ChooseConectionPool {

	public enum TYPE {
		
		CACHE(1),FIXED(2),ONLY_CACHE(3),ONLY_NEW(4);
		
		private int type = 0;
		
		TYPE(int type){
			this.type = type;
		}
		
		private int getType(){
			return type;
		}
	};
	
	public static IBuilder getIBuilder(Deploy deploy) throws ClassNotFoundException, IllegalAccessException, SQLException, IOException{
		int type = get(deploy);
		if(type == TYPE.CACHE.getType()){
			return new CacheConnectionPool.Builder(deploy);
		} else if (type == TYPE.FIXED.getType()){
			return new FixedConnectionPool.Builder(deploy);
		} else if(type == TYPE.ONLY_CACHE.getType()){
			return new OnlyCacheConnectionPool.Builder(deploy);
		} else if(type == TYPE.ONLY_NEW.getType()){
			return new OnlyNewConnectionPool.Builder(deploy);
		}
		return null;
	}
	
	private static int get(Deploy aDeploy){
		if (aDeploy.getCacheSize()  <= 0 && aDeploy.getNewMaxSize() <= 0){
			return TYPE.CACHE.getType();
		}
		if (aDeploy.getCacheSize() > 0 && aDeploy.getNewMaxSize() > 0){
			return TYPE.FIXED.getType();
		}
		if (aDeploy.getCacheSize() > 0 && aDeploy.getNewMaxSize() <= 0){
			return TYPE.ONLY_CACHE.getType();
		}
		if (aDeploy.getCacheSize() <= 0 && aDeploy.getNewMaxSize() > 0){
			return TYPE.ONLY_NEW.getType();
		}	
		return 0;
	}

}
