 **数据库连接池XDCPool对连接提供了四种不同模式的管理，对于不同的访问量我们可以选择不同的管理模式，并且自动回收连接，我们先看下如何使用。** 

XDCPool使用起来非常简单，几乎零配置，看代码：
```
Deploy mDeploy = new Deploy()
                .setUrl("jdbc:mysql://127.0.0.1") 
                .setUser("root")
                .setPassword("root"）
                //DriverClassName.MySQL是加载MySql的驱动字符串，当然我们也可以替换为“com.mysql.jdbc.Driver”
                .setDriverClassName(DriverClassName.MySQL)
                //设置缓存连接的数量
                .setCacheSize(10)
                //设置最大新建连接的数量
                .setNewMaxSize(500)
                //是否打印日志
                .setCloseLog(true);
XDCPool.init(mDeploy);
```

这就完成初始化了(初始化只需一次)，通过链式调用进行配置，是不是很简单呢。

XDCPool为我们提供了简单的操作数据库的方法，分别是：
```
//查询操作
XDCPool.delete(String sql,String[] indexs);
//更新操作
XDCPool.update(String sql,String[] indexs);
//查询操作
XDCPool.select(String sql,String[] indexs);
//插入操作
XDCPool.insert(String sql,String[] indexs);
使用如下：

//删除操作
XDCPool.delete("DELETE FROM ? WHERE id=?",new String[]{"student","1"});
//更新操作
XDCPool.update("UPDATE ? SET name = ? WHERE id = ?",new String[]{"student","张三","1"});
//查询操作
XDCPool.select("SELECT * FROM student",null);
//插入操作
XDCPool.insert("INSERT INTO student VALUES (?,?)",new String[]{"1","张三"});
```

当我们对数据库进行增删改查时尽量使用XDCPool为我们提供的这四种方法，因为它会自动回收我们的连接，也不需要我们手动进行关闭。

如果说我们想自己拿到连接进行处理，该怎么办呢？我们可以通过调用下面的方法拿到连接(注意使用完之后，务必调用XDCPool.release(mConnection)进行手动回收)：

Connection mConnection = XDCPool.getConnection();
如果你想随时查看连接池中的连接的数量，我们可以调用下面的方法：
```
//得到缓存池中的数量
XDCPool.getCacheConnectionCount();
//得到当前新建的连接的数量
XDCPool.getNewConnectionCount();
当然还有关闭连接池的方法：
```


XDCPool.closeConnectionPool();
上面说过XDCPool有四种模式可以选择，这里怎么没有配置呢？其实模式的配备不需要我们手动设置，XDCPool会根据我们setCacheSize()方法和setNewMaxSize()自动匹配。


1. 1、设置了缓存数量和最大新建连接数量：那么XDCPool在初始化时会新建我们指定的连接数量，当我们需要使用连接时随时可以拿到连接。当程序遇到高并发时，它会快速新建大量新的连接应付高并发，当然这是有限制的，新建的连接数受我们指定最大新建连接数的制约。
1. 2、设置了缓存数量但没有设置最大新建连接数量：那么XDCPool在初始化时会新建我们指定的连接数量，并定期会检查连接是否可用，当我们的程序遇到高并发时，它会快速创建大量新的连接应付高并发（无限制）。当我们的程序恢复正常时，XDCPool保留指定的连接数量，多余的连接会替我们关闭。
1. 3、设置了最大新建连接数量但没有设置缓存数量：那么XDCPool在初始化时不创建连接，当程序需要使用连接时会及时创建连接，连接使用完毕并不会被XDCPool及时关闭掉，它会保留一分钟，在这一分钟内，如果需要再次连接数据库，那么这个连接就会被复用。当我们的程序遇到高并发时，和模式1相同，它会快速创建大量新的连接应付高并发，但受我们指定最大新建连接数的制约。
1. 4、没有设置最大新建连接数量和缓存的数量：那么XDCPool在初始化时不创建连接，当程序需要使用连接时会及时创建连接，连接使用完毕并不会被XDCPool及时关闭掉，它会保留一分钟，在这一分钟内，如果需要再次连接数据库，那么这个连接就会被复用。当我们的程序遇到高并发时，它会快速创建大量新的连接应付高并发（无限制）。